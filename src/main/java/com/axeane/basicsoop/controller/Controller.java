package com.axeane.basicsoop.controller;

public interface Controller {
  void listOfRoomsBySearch(int price, String district, int metersToSchool,
      int metersToShop, int metersToBusStation, int bedCount);
  void listOfHousesBySearch(int price, String district, int metersToSchool,
      int metersToShop, int metersToBusStation, int squareMeters);
  void listOfApartmentsBySearch(int price, String district, int metersToSchool,
      int metersToShop, int metersToBusStation, int numberOfRooms);
}
