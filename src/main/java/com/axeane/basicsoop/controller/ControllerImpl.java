package com.axeane.basicsoop.controller;

import com.axeane.basicsoop.model.Apartment;
import com.axeane.basicsoop.model.BusinessLogicImpl;
import com.axeane.basicsoop.model.House;
import com.axeane.basicsoop.model.Room;
import java.util.List;

public class ControllerImpl implements Controller {
  private BusinessLogicImpl businessLogicImpl = new BusinessLogicImpl();

  @Override
  public void listOfRoomsBySearch(int price, String district, int metersToSchool,
      int metersToShop, int metersToBusStation, int bedCount) {
    List<Room> listOfRoomsBySearch = businessLogicImpl
        .listOfRoomsBySearchParam(price, district, metersToSchool,
            metersToShop, metersToBusStation, bedCount);
    if (listOfRoomsBySearch.size() > 0) {
      System.out.println("\nList of available houses: ");
      for (Room ofRoomsBySearch : listOfRoomsBySearch) {
        System.out.println(ofRoomsBySearch.toString());
      }
    } else {
      System.out.println("\nThere is no available rooms by your criteria");
    }

  }

  @Override
  public void listOfHousesBySearch(int price, String district, int metersToSchool,
      int metersToShop, int metersToBusStation, int squareMeters) {
    List<House> listOfHousesBySearch = businessLogicImpl
        .listOfHousesBySearchParam(price, district, metersToSchool,
            metersToShop, metersToBusStation, squareMeters);
    if (listOfHousesBySearch.size() > 0) {
      System.out.println("\nList of available houses: ");
      for (House ofHousesBySearch : listOfHousesBySearch) {
        System.out.println(ofHousesBySearch.toString());
      }
    } else {
      System.out.println("\nThere is no available houses by your criteria");
    }
  }

  @Override
  public void listOfApartmentsBySearch(int price, String district, int metersToSchool,
      int metersToShop, int metersToBusStation, int numberOfRooms) {
    List<Apartment> listOfApartmentsBySearch = businessLogicImpl
        .listOfApartmentsBySearchParam(price, district, metersToSchool,
            metersToShop, metersToBusStation, numberOfRooms);
    if (listOfApartmentsBySearch.size() > 0) {
      System.out.println("\nList of available apartments: ");
      for (Apartment ofApartmentsBySearch : listOfApartmentsBySearch) {
        System.out.println(ofApartmentsBySearch.toString());
      }
    } else {
      System.out.println("\nThere is no available apartments by your criteria");
    }
  }
}
