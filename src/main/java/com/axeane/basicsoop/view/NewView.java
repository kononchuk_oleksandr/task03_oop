package com.axeane.basicsoop.view;

import com.axeane.basicsoop.controller.Controller;
import com.axeane.basicsoop.controller.ControllerImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

public class NewView {
  private BufferedReader reader = new BufferedReader(
      new InputStreamReader(System.in, StandardCharsets.UTF_8));
  private Controller controller = new ControllerImpl();
  private Map<String, String> menuMap;
  private Map<String, Printable> methodsMap;
  private Map<String, String> districtMap;
  private int price;
  private String district;
  private int metersToSchool;
  private int metersToShop;
  private int metersToBusStation;

  public NewView() {
    menuMap = new LinkedHashMap<>();
    menuMap.put("1", "1 - Rent a room");
    menuMap.put("2", "2 - Rent a house");
    menuMap.put("3", "3 - Rent an apartment");
    menuMap.put("E", "E - Exit");

    methodsMap = new LinkedHashMap<>();
    methodsMap.put("1", this::button1);
    methodsMap.put("2", this::button2);
    methodsMap.put("3", this::button3);

    districtMap = new LinkedHashMap<>();
    districtMap.put("1", "1 - Halych");
    districtMap.put("2", "2 - Railway");
    districtMap.put("3", "3 - Lychakiv");
    districtMap.put("4", "4 - Sykhiv");
    districtMap.put("5", "5 - Franko");
    districtMap.put("6", "6 - Shevchenko");
  }

  private void button1() throws IOException {
    getParameters();
    int bedCount = Integer.parseInt(reader.readLine());
    controller.listOfRoomsBySearch(price, district, metersToSchool, metersToShop, metersToBusStation, bedCount);
  }

  private void button2() throws IOException {
    getParameters();
    int squareMeters = Integer.parseInt(reader.readLine());
    controller.listOfHousesBySearch(price, district, metersToSchool, metersToShop, metersToBusStation, squareMeters);
  }

  private void button3() throws IOException{
    getParameters();
    int numberOfRooms = Integer.parseInt(reader.readLine());
    controller.listOfApartmentsBySearch(price, district, metersToSchool, metersToShop, metersToBusStation, numberOfRooms);
  }

  private void menu() {
    for (String str : menuMap.values()) {
      System.out.println(str);
    }
  }

  private String getDistrict() throws IOException {
    int districtId;
    String text = null;
    System.out.println("Please choose a name of district: ");
    for (String str: districtMap.values()) {
      System.out.println(str);
    }
    districtId = Integer.parseInt(reader.readLine());
    switch (districtId) {
      case 1:
        text = "Halych";
        break;
      case 2:
        text = "Railway";
        break;
      case 3:
        text = "Lychakiv";
        break;
      case 4:
        text = "Sykhiv";
        break;
      case 5:
        text = "Franko";
        break;
      case 6:
        text = "Shevchenko";
        break;
    }
    return text;
  }

  private void getParameters() throws IOException {
    System.out.print("Please enter the price: ");
    price = Integer.parseInt(reader.readLine());
    district = getDistrict();
    System.out.print("Please enter a maximum number of meters to a school: ");
    metersToSchool = Integer.parseInt(reader.readLine());
    System.out.print("Please enter a maximum number of meters to a shop: ");
    metersToShop = Integer.parseInt(reader.readLine());
    System.out.print("Please enter a maximum number of meters to a bus station: ");
    metersToBusStation = Integer.parseInt(reader.readLine());
    System.out.print("Please enter the number of beds: ");
  }

  public void start() {
    String key = null;
    do {
      System.out.println("\nWelcome to the \"Rent in Lviv\" application! \nPress the button:");
      menu();
      try {
        key = reader.readLine();
        methodsMap.get(key).print();
      } catch (Exception e) {}

    } while (!key.equalsIgnoreCase("E"));
  }
}
