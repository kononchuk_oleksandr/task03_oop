package com.axeane.basicsoop.view;

import java.io.IOException;

@FunctionalInterface
public interface Printable {

  void print() throws IOException;

}
