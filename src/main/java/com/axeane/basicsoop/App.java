package com.axeane.basicsoop;

import com.axeane.basicsoop.view.NewView;

public class App {
  public static void main(String[] args) {
    new NewView().start();
  }
}
