package com.axeane.basicsoop.model;

public class Apartment extends Building{
  private int numberOfRooms;

  public Apartment(int price, District district, int metersToSchool,
      int metersToShop, int metersToBusStation, int numberOfRooms) {
    super(price, district, metersToSchool, metersToShop, metersToBusStation);
    this.numberOfRooms = numberOfRooms;
  }

  public int getNumberOfRooms() {
    return numberOfRooms;
  }

  @Override
  public String toString() {
    return "Apartment for price " + getPrice() + " UAH with " +
        getNumberOfRooms() + " room(s) at " +
        getDistrict().getTitle() + " district in " +
        getMetersToBusStation() + " meters to bus station, " +
        getMetersToShop() + " meters to shop and " +
        getMetersToSchool() + " meters to school.";
  }
}
