package com.axeane.basicsoop.model;

import java.util.List;

public interface BusinessLogic {
  List<Room> listOfRooms();
  List<House> listOfHouses();
  List<Apartment> listOfApartments();
  List<Room> listOfRoomsBySearchParam(int price, String district,
      int metersToSchool, int metersToShop, int metersToBusStation,
      int bedCount);
  List<House> listOfHousesBySearchParam(int price, String district,
      int metersToSchool, int metersToShop, int metersToBusStation,
      int squareMeters);
  List<Apartment> listOfApartmentsBySearchParam(int price, String district,
      int metersToSchool, int metersToShop, int metersToBusStation,
      int numberOfRooms);
}
