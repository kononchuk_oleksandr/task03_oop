package com.axeane.basicsoop.model;

public class House extends Building{
  private int squareMeters;

  public House(int price, District district, int metersToSchool,
      int metersToShop, int metersToBusStation, int squareMeters) {
    super(price, district, metersToSchool, metersToShop, metersToBusStation);
    this.squareMeters = squareMeters;
  }

  public int getSquareMeters() {
    return squareMeters;
  }

  @Override
  public String toString() {
    return "House with price " + getPrice() + " UAH with " +
        getSquareMeters() + " square meters at " +
        getDistrict().getTitle() + " district in " +
        getMetersToBusStation() + " meters to bus station, " +
        getMetersToShop() + " meters to shop and " +
        getMetersToSchool() + " meters to school.";
  }
}
