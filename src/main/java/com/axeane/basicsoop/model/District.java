package com.axeane.basicsoop.model;

public enum District {
  HALYCH ("Halych"),
  RAILWAY ("Railway"),
  LYCHAKIV ("Lychakiv"),
  SYKHIV ("Sykhiv"),
  FRANKO ("Franko"),
  SHEVCHENKO ("Shevchenko");

  private String title;

  District(String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }
}
