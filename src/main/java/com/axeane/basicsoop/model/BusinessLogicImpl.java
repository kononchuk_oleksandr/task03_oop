package com.axeane.basicsoop.model;

import java.util.ArrayList;
import java.util.List;

public class BusinessLogicImpl implements BusinessLogic {

  @Override
  public List<Room> listOfRooms() {
    List<Room> listOfRooms = new ArrayList<>();
    listOfRooms.add(new Room(500, District.FRANKO, 550, 170, 230, 2));
    listOfRooms.add(new Room(650, District.SYKHIV, 400, 320, 400, 1));
    listOfRooms.add(new Room(650, District.HALYCH, 300, 670, 500, 2));
    listOfRooms.add(new Room(650, District.SHEVCHENKO, 200, 300, 300, 3));
    listOfRooms.add(new Room(1, District.HALYCH, 1, 1, 1, 1));
    listOfRooms.add(new Room(1, District.RAILWAY, 1, 1, 1, 1));
    listOfRooms.add(new Room(1, District.LYCHAKIV, 1, 1, 1, 1));
    listOfRooms.add(new Room(1, District.SYKHIV, 1, 1, 1, 1));
    listOfRooms.add(new Room(1, District.FRANKO, 1, 1, 1, 1));
    listOfRooms.add(new Room(1, District.SHEVCHENKO, 1, 1, 1, 1));
    return listOfRooms;
  }

  @Override
  public List<House> listOfHouses() {
    List<House> listOfHouses = new ArrayList<>();
    listOfHouses.add(new House(100000, District.SHEVCHENKO, 800, 400, 1000, 75));
    listOfHouses.add(new House(75000, District.HALYCH, 400, 270, 100, 60));
    listOfHouses.add(new House(250000, District.LYCHAKIV, 300, 300, 600, 160));
    listOfHouses.add(new House(40000, District.RAILWAY, 200, 600, 300, 40));
    listOfHouses.add(new House(1, District.HALYCH, 1, 1, 1, 1));
    listOfHouses.add(new House(1, District.RAILWAY, 1, 1, 1, 1));
    listOfHouses.add(new House(1, District.LYCHAKIV, 1, 1, 1, 1));
    listOfHouses.add(new House(1, District.SYKHIV, 1, 1, 1, 1));
    listOfHouses.add(new House(1, District.FRANKO, 1, 1, 1, 1));
    listOfHouses.add(new House(1, District.SHEVCHENKO, 1, 1, 1, 1));
    return listOfHouses;
  }

  @Override
  public List<Apartment> listOfApartments() {
    List<Apartment> listOfApartments = new ArrayList<>();
    listOfApartments.add(new Apartment(4000, District.FRANKO, 550, 170, 230, 3));
    listOfApartments.add(new Apartment(5000, District.SYKHIV, 400, 320, 400, 1));
    listOfApartments.add(new Apartment(6000, District.HALYCH, 300, 670, 500, 2));
    listOfApartments.add(new Apartment(7000, District.SHEVCHENKO, 200, 300, 300, 3));
    listOfApartments.add(new Apartment(1, District.HALYCH, 1, 1, 1, 1));
    listOfApartments.add(new Apartment(1, District.RAILWAY, 1, 1, 1, 1));
    listOfApartments.add(new Apartment(1, District.LYCHAKIV, 1, 1, 1, 1));
    listOfApartments.add(new Apartment(1, District.SYKHIV, 1, 1, 1, 1));
    listOfApartments.add(new Apartment(1, District.FRANKO, 1, 1, 1, 1));
    listOfApartments.add(new Apartment(1, District.SHEVCHENKO, 1, 1, 1, 1));
    return listOfApartments;
  }

  @Override
  public List<Room> listOfRoomsBySearchParam(int price, String district,
      int metersToSchool, int metersToShop, int metersToBusStation,
      int bedCount) {
    List<Room> list = listOfRooms();
    List<Room> listOfRoomsBySearch = new ArrayList<>();
    for (Room room : list) {
      if (room.getPrice() <= price &&
          room.getDistrict().getTitle().equals(district) &&
          room.getMetersToSchool() <= metersToSchool &&
          room.getMetersToShop() <= metersToShop &&
          room.getMetersToBusStation() <= metersToBusStation &&
          room.getBedCount() <= bedCount) {
        listOfRoomsBySearch.add(room);
      }
    }
    return listOfRoomsBySearch;
  }

  @Override
  public List<House> listOfHousesBySearchParam(int price, String district, int metersToSchool,
      int metersToShop, int metersToBusStation, int squareMeters) {
    List<House> list = listOfHouses();
    List<House> listOfHousesBySearch = new ArrayList<>();
    for (House house : list) {
      if (house.getPrice() <= price &&
          house.getDistrict().getTitle().equals(district) &&
          house.getMetersToSchool() <= metersToSchool &&
          house.getMetersToShop() <= metersToShop &&
          house.getMetersToBusStation() <= metersToBusStation &&
          house.getSquareMeters() <= squareMeters) {
        listOfHousesBySearch.add(house);
      }
    }
    return listOfHousesBySearch;
  }

  @Override
  public List<Apartment> listOfApartmentsBySearchParam(int price, String district,
      int metersToSchool, int metersToShop, int metersToBusStation, int numberOfRooms) {
    List<Apartment> list = listOfApartments();
    List<Apartment> listOfApartmentsBySearch = new ArrayList<>();
    for (Apartment apartment : list) {
      if (apartment.getPrice() <= price &&
          apartment.getDistrict().getTitle().equals(district) &&
          apartment.getMetersToSchool() <= metersToSchool &&
          apartment.getMetersToShop() <= metersToShop &&
          apartment.getMetersToBusStation() <= metersToBusStation &&
          apartment.getNumberOfRooms() <= numberOfRooms) {
        listOfApartmentsBySearch.add(apartment);
      }
    }
    return listOfApartmentsBySearch;
  }
}
