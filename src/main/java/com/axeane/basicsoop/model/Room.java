package com.axeane.basicsoop.model;

public class Room extends Building {
  private int bedCount;

  public Room(int price, District district, int metersToSchool,
      int metersToShop, int metersToBusStation, int bedCount) {
    super(price, district, metersToSchool, metersToShop, metersToBusStation);
    this.bedCount = bedCount;
  }

  public int getBedCount() {
    return bedCount;
  }

  public void setBedCount(int bedCount) {
    this.bedCount = bedCount;
  }

  @Override
  public String toString() {
    return "Room for price " + getPrice() + " UAH with " +
        getBedCount() + " bed(s) at " +
        getDistrict().getTitle() + " district in " +
        getMetersToBusStation() + " meters to bus station, " +
        getMetersToShop() + " meters to shop and " +
        getMetersToSchool() + " meters to school.";
  }
}
