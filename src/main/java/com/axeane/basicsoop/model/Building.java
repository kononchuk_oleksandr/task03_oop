package com.axeane.basicsoop.model;

public abstract class Building {
  private int price;
  private District district;
  private int metersToSchool;
  private int metersToShop;
  private int metersToBusStation;

  public Building(int price, District district, int metersToSchool,
      int metersToShop, int metersToBusStation) {
    this.price = price;
    this.district = district;
    this.metersToSchool = metersToSchool;
    this.metersToShop = metersToShop;
    this.metersToBusStation = metersToBusStation;
  }

  int getPrice() {
    return price;
  }

  public District getDistrict() {
    return district;
  }

  int getMetersToSchool() {
    return metersToSchool;
  }

  int getMetersToShop() {
    return metersToShop;
  }

  int getMetersToBusStation() {
    return metersToBusStation;
  }
}
